@extends('simplemodule::layouts.master')

@section('content')
    <h1>Tampilan Kesan dan Pesanmu</h1>
    
    <div class="container col-6">
        <div class="form-group">
            <label for="pesanTextArea">Kesan</label>
            <textarea class="form-control" id="pesanTextArea" readonly name="kesan" rows="3" value="{{ $pesan->kesan }}">{{ $pesan->kesan }}</textarea>
        </div>
        <div class="form-group">
            <label for="pesanTextArea">Pesan</label>
            <textarea class="form-control" id="pesanTextArea" readonly name="pesan" rows="3" value="{{ $pesan->pesan }}">{{ $pesan->pesan }}</textarea>
        </div>

        <div class="row">
            <div class="col-1 mr-1">                
                <a href="/simplemodule/pesan/{{ $pesan->id }}/edit">
                    <button class="btn btn-primary" type="submit">Edit</button>
                </a>
            </div>
            <div class="col-1 mr-2">
                <form action="/simplemodule/pesan/{{ $pesan->id }}" method="POST">
                    @method('DELETE')
                    @csrf
                    
                    <button class="btn btn-danger">Delete</button>
                </form>
            </div>
            <div class="col-1 ml-3">                
                <a href="/simplemodule/">
                    <button class="btn btn-secondary">Kembali</button>
                </a>
            </div>
        </div>
    </div>

@endsection
