<?php

namespace Modules\SimpleModule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Modules\SimpleModule\Entities\Pesan;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Support\Renderable;
use Modules\SimpleModule\Emails\TestimonialReceived;
use Modules\SimpleModule\Jobs\SendMailTestimonialReceived;
use Symfony\Component\HttpFoundation\Test\Constraint\ResponseStatusCodeSame;

class SimpleModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $pesan = Pesan::all();
        if ($pesan->isEmpty()==false) {
            return response()->json([
                'Status' => 'OK', 
                'data' => $pesan
            ],200);
        }
        elseif($pesan->isEmpty()==true) {            
            return response()->json([
                'Status' => 'NotFound',
                'messages' => 'Tidak ada data yang dapat ditampilkan'
            ],404);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    // public function create()
    // {
    //     return view('simplemodule::pesan.create');
    // }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pesan'=>'required',
            'kesan'=>'required',
            'email'=>'required',
        ]);
        if ($validator->errors()->isEmpty()==true) {
            $pesan = Pesan::create([
                'kesan' => $request->kesan,
                'pesan' => $request->pesan,
                'email' => $request->email,
            ]);
            $this->_sendEmailTestimonialReceived($pesan);
            return response()->json([
                'Status' => 'OK', 
                'messages' => 'Data berhasil ditambahkan',
                'data' => $pesan
            ],200);
        }
        elseif ($validator->errors()->isEmpty()==false) {
            return response()->json([
                'Status' => 'BadRequest', 
                'messages' => 'Data gagal ditambahkan',
                'errors'=>$validator->errors()
            ],400);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show(Request $request, $id)
    {
        $messeges = new Pesan;
        $getData = $messeges->firstWhere('id', $id);
        if ($getData) {
            return response()->json([
                'Status' => 'OK', 
                'data' => [
                    'id' => $getData->id,
                    'kesan' => $getData->kesan,
                    'pesan' => $getData->pesan,
                    'created_at' => $getData->created_at,
                    'updated_at' => $getData->updated_at,
                ]
            ],200);
        }
        else {            
            return response()->json([
                'Status' => 'NotFound', 
                'messages' => 'Data tidak ditemukan'
            ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    // public function edit(Pesan $pesan)
    // {
    //     return view('simplemodule::pesan.edit', compact('pesan'));
    // }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'pesan'=>'required',
            'kesan'=>'required'
        ]);
        $pesan = new Pesan;
        if ($validator->fails()==false) {
            $pesanUpdate = $pesan->where('id',$id)
            ->update([
                'kesan' => $request->kesan,
                'pesan' => $request->pesan,
            ]);
            return response()->json([
                'Status' => 'OK', 
                'messages' => 'Data berhasil di update',
                'data' => $pesan->findOrFail($id)
            ],200);
        }
        elseif ($validator->fails()==true) {
            return response()->json([
                'Status' => 'BadRequest', 
                'messages' => 'Data gagal di update',
                'errors'=>$validator->errors()
            ],400);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy(Request $request, $id)
    {
        $messeges = new Pesan;
        $getData = $messeges->firstWhere('id', $id) ;
        if (!empty($getData)) {
            $deleted = $getData->delete();
            return response()->json([
                'Status' => 'OK', 
                'messages' => 'Data berhasil di hapus'
            ],200);
        }
        elseif (empty($getData)) {            
            return response()->json([
                'Status' => 'NotFound', 
                'messages' => 'Data tidak ditemukan'
            ],404);
        }
    }
    private function _sendEmailTestimonialReceived($pesan){
        SendMailTestimonialReceived::dispatch($pesan);
    }
}
