<?php

namespace Modules\SimpleModule\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Modules\SimpleModule\Entities\Pesan;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Modules\SimpleModule\Emails\TestimonialReceived;

class SendMailTestimonialReceived implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $testimonials;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($testimonials)
    {
        $this->testimonials = $testimonials;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $testimonialReceivedEmail = new TestimonialReceived($this->testimonials);
        Mail::to(Pesan::first()->email)->send($testimonialReceivedEmail);
        Telegram::sendMessage([
            'chat_id' => '@testimonialsapp',
            'text' => 'Ada testimonial baru dari: ' . Pesan::first()->email . ' dengan kesan: ' . Pesan::first()->kesan . ' dan pesan: ' . Pesan::first()->pesan,
        ]);
    }
}
